# Cloud Event



## init
```shell
docker run --rm -it  -v ./:/app registry.cn-hangzhou.aliyuncs.com/wylibrary/cncfstack-vitepress:v1.0  /bin/sh
cd /app
npx vitepress init
```

## build
```
docker run --rm -it  -v ./:/app registry.cn-hangzhou.aliyuncs.com/wylibrary/cncfstack-vitepress:v1.0 -- npm run docs:build
···
